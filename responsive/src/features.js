
import mjlogo from './images/mjlogo.jpg';
import { FeaturesSection, ContainerDiv, FeatureDiv, CompanyLogo, FeatureDivImg} from "./design";

const Features = () => {

    return (

        <FeaturesSection>
            <ContainerDiv>
               
                <FeatureDiv>
                    <CompanyLogo src={ mjlogo } />
                    <p>This is Company Logo</p>
                </FeatureDiv>

                <FeatureDiv>
                    <FeatureDivImg src="https://www.sololearn.com/uploads/img_blue_pin.png" />
                    <p>This is features</p>
                </FeatureDiv>
                    
                <FeatureDiv>
                    <FeatureDivImg src="https://www.sololearn.com/uploads/img_blue_chart.png" />
                    <p>This is features</p>
                </FeatureDiv>

                <FeatureDiv>
                    <FeatureDivImg src="https://www.sololearn.com/uploads/img_blue_msg.png" />
                    <p>This is features</p>
                </FeatureDiv>

            </ContainerDiv>
        </FeaturesSection>
    );

};

export default Features;