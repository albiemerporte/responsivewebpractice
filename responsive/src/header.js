import { HeaderSection, ContainerDiv, ButtonSection }  from './design';

import menu from './images/menu.svg';

const Myheader = () => {
    return(
        <HeaderSection>
            <ContainerDiv>
                <nav class="menu">
                    <ul class="menu-list">
                        <li className="menuclick">
                            <img src={ menu } alt="menu-icon" style={{width:'60px', height:'60px'}}/></li>
                        <li><a className="headeropt" href="#headeropt">header</a></li>
                        <li><a className="featuresopt" href="">Feature</a></li>
                        <li><a className="qouteopt" href="">Qoute</a></li>
                        <li><a className="footeropt" href="#footeropt">Footer</a></li>
                    </ul>
                </nav>
                <h1>HI, I am Albiemer Porte</h1>
                <h2>This App will change your life</h2>
                <ButtonSection>MENU</ButtonSection>
            </ContainerDiv>
        </HeaderSection>
    );
};

export default Myheader;