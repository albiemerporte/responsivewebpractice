
import styled, { createGlobalStyle } from 'styled-components';

// <Globe /> i App.js
export const Globe = createGlobalStyle`

    @import url(//fonts.googleapis.com/css?family=Open+Sans:400,700,800,300);

`;

// <Body> in App.js
export const Body = styled.body`

        margin: 0;
	    padding: 0;
	    font-family: 'Open Sans', 'Helvetica Neue', Helvetica,sans-serif;
	    line-height: 1.45;

`;

// <HeaderSection> in header.js
export const HeaderSection = styled.header.attrs(props => ({

    id: props.id || 'headeropt',

}))`

    color: #FFFFFF;
	background-color: #5294e2;
	padding: 80px 0;
	text-align: center;

    .menu {
        /*background: #d0a900;*/
        left: 0;
        position: absolute;
        left: -25px;
        transition: 0.3s;
        padding: 20px;
        width: 60px;
        text-decoration: none;
        font-size: 20px;
        color: white;
        border-radius: 0 5px 5px 0;
        position: fixed;
        transition-delay: 0.3s;

    }

    .menuclick {
        display: block;

    }



    .menu:hover{
        flex-direction: column;
        background: #d0a900;
        width: 500px;
        text-align: left;
        display: block;
        color: #e50c6f;

        .menu-list {
            border-top: 1px solid green;
        }

        .headeropt, .featuresopt, .qouteopt, .footeropt{
            display:block;
            padding: 5px;
            margin-top: 40px;
        } 

        .menuclick {
            display: none;
        }

        li {
            display: block;

            a:hover {
                color: #e50c6f;
            }
        }

    }

    ul {
        
        list-style: none;
        margin: 0;
        padding: 0;
        display: flex;
        justify-content: space-between;
    }

    li {
        margin: 0 10px;
        display: none;
    }
      
    li a {
        text-decoration: none;
        color: #333;
        padding: 5px;
    }

    /*.about, .services, .portfolio, .contact{
        display:none;
    } */


    @media screen and (max-width: 480px){

        padding: 25px 0 15px 0;
        border: solid 1px red;

        .menuclick {
            display: block;
        }

        .menu:hover {
            width: 90px;

            li {
                display: block;
            }

            .about {
                margin-top: 30px;
            }

        }

        h1 {
            font-size: 32px;
            margin: 0 0 8px 0;
        }
        h2 {
            font-size: 18px;
        }

        ul {
            flex-direction: column;
        }

        li {
            display: none;
        }
    
    }

`;

// <ContainerDiv> in header.js, features.js, qoute.js, footer.js
export const ContainerDiv = styled.div`

    margin: 0 auto;
    padding: 0 20px 0 20px;
    max-width: 900px;

`;

// <ButtonSection> in header.js
export const ButtonSection = styled.a`

    display: inline-block;
    color: #ffffff;
    font-weight: 500;
    font-size: 20px;
    Background: #549da0;
    border: none;
    border-radius: 5px;
    padding: 12px 16px;
    cursor: pointer;

    &:hover {

        background: #468486;

    }

    @media screen and (max-width: 480px){

        {
            display: block;
            font-size: 18px;
        }
    }

`;

// <FeaturesSection> in features.js
export const FeaturesSection = styled.section`

    padding: 80px 0;
    text-align: center;
    background: #ffffff;
    color: #000000;

`;

// <FeatureDiv> in features.js
export const FeatureDiv = styled.div`

    width: 32%;
    display: inline-block;
    fonr-size: 16px;

    @media screen and (max-width: 480px) {

        width: 100%;
        text-align: left;
        margin: 0 0 10px 0;
        font-size: 16px;
        display: flex;
        align-items: center;
        justify-content: center;

    }

`;

// <CompanyLogo> in features.js 
export const CompanyLogo = styled.img`

    width: 40%;

    @media screen and (max-width: 480px) {
        width:15%;
        min-width:60px;
        margin-right:20px;
    }

`;


// <FeatureDivImg> in fetures.js
export const FeatureDivImg = styled.img`

    width: 40%; 
    
    @media screen and (max-width: 480px) {
        width:15%;
        min-width:60px;
        margin-right:20px;
    }   

`;

// <QouteSection> in qoute.js
export const QouteSection = styled.section`

    padding: 40px 0;
    text-align: center;
    background: #549DA0;
    color: #FFFFFF;
    font-size: 30px;

`;

// <QouteContainer> in qoute.js
export const QouteContainer = styled.blockquote`

    margin: 0 auto;
    padding: 0 20px 0 20px;
    max-width: 900px;
    text-align: center;

    p {
        margin: 0 0 5px 0;
        font-size: 24px;
    }

    cite {
        content:'-';
        margin-right: 5px;
    

        &:after {
            font-size: 16px;
            font-style: italic;
        }
    }

`;


// <FooterSection> in footer.js
export const FooterSection = styled.footer.attrs(props => ({

    id: props.id || 'footeropt',

}))`

    background: #353535;
    padding: 30px 0 10px 0;
    text-align: center;
    color: #868686;

    .container {
        margin: 0 auto;
        padding: 0 20px 0 20px;
        max-width: 900px;

    }

    ul {
        margin: 0;
        padding 0;
        list-style: none;
    }

    li {
        display: block;
        margin: 3px 0;
    }

    a {
        padding: 6px;
        font-size:14px;
        text-decoration: none;
        color: #c3c3c3;
        

        &:hover {
            color: #a3719f;
        }

    }

`;