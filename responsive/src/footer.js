
import { FooterSection } from "./design";

const Myfooter = () => {

    return (
        <FooterSection>
            <div className = "container">
                <ul>
                    <li><a>Home</a></li>
                    <li><a>About</a></li>
                    <li><a>Contacts</a></li>
                </ul>
                <p>&copy; All rights reserved</p>
            </div>
        </FooterSection>
    );

};

export default Myfooter;