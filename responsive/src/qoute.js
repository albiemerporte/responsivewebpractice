
import { QouteSection, QouteContainer } from "./design";

const Qoute = () => {

    return (
        <QouteSection>
            <QouteContainer>
                <p>"Some Great Qoute"</p>
                <cite>Satisfied Customer</cite>
            </QouteContainer>
        </QouteSection>
    );

};

export default Qoute;