
import Myheader from "./header";
import Features from './features';
import Qoute from './qoute';
import Myfooter from './footer';
import { Body, Globe } from './design';
import { BrowserRouter } from "react-router-dom";

function App() {
  return (
    <>
      <Globe />
        <Body>
          <BrowserRouter>
            <Myheader />
            <Features />
            <Qoute />
            <Myfooter />
          </BrowserRouter>
    
        </Body>
    </>
  );
}

export default App;
